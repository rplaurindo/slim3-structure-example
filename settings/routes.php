<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->group('/hello-world', function () {
    // test with http://localhost/slim3-structure-example/public/hello-world
    $this->get('', function (Request $request, Response $response, array $args) {
        // $request->getParams() to get query params;
        // json_encode();
        // $args[''] to get path params
        $aServiceExample = $this->get('AServiceExample');
        echo $aServiceExample->sayHelloTo('there');

    });
});
