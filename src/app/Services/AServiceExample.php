<?php

namespace Services;

class AServiceExample {

    public $aProperty = 'a property value';
    public $smtp = 'a property value';

    private $iterator;

    function __construct() {
        $mailSendingProxy = new MailSendingProxyService();
        $mailSendingProxy->addTo('rafaelplaurindo@gmail.com');
        $mailSendingProxy->setSubject('Subject test');
        $mailSendingProxy->setBody('Body test');
        $mailSendingProxy->send();

        $arr = array(
            'a' => array(
                'a.1' => array(
                    'a.1.1' => 'a.1.1',
                    'a.1.2' => 'a.1.2'
                ),
                'a.2' => array(
                    'a.2.1' => 'a.2.1',
                    'a.2.2' => 'a.2.2'
                )
            ),
            'b' => array(
                'b.1' => array(
                    'b.1.1' => 'b.1.1',
                    'b.1.2' => 'b.1.2'
                ),
                'b.2' => 'b.2'
            )
        );

//        $this->iterator = new RecursiveIteratorService($arr);
    }

    function sayHelloTo($name) {
        return "Hello {$name}.";

//        $this->iterator->printIteration();
    }

}
