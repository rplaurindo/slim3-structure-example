<?php

$rootPath = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..');
$settingsPath = $rootPath . DIRECTORY_SEPARATOR . 'settings';

require $settingsPath . DIRECTORY_SEPARATOR . 'loadPath.php';
include 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

//to differ from another file with the same name
require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

session_start();

require 'src' . DIRECTORY_SEPARATOR . 'autoload.php';

// Instantiate the app
$settings = require 'settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require 'dependencies.php';

// Register middleware
require 'middleware.php';

// Register routes
require $settingsPath . DIRECTORY_SEPARATOR . 'routes.php';

// Run app
try {
    $app->run();
} catch(Exception $e) {
    echo $e;
}
